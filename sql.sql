﻿-- Table: public.person

-- DROP TABLE public.person;

CREATE TABLE public.person
(
  id integer NOT NULL DEFAULT nextval('person_id_seq'::regclass),
  age integer NOT NULL,
  name text NOT NULL,
  id_city integer,
  CONSTRAINT "PKey" PRIMARY KEY (id)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE public.person
  OWNER TO postgres;


-- Table: public.detail

-- DROP TABLE public.detail;

CREATE TABLE public.detail
(
  "ID" integer,
  "NAME" text,
  "ID_PERSON" integer
)
WITH (
  OIDS=FALSE
);
ALTER TABLE public.detail
  OWNER TO postgres;

-- Table: public.city

-- DROP TABLE public.city;

CREATE TABLE public.city
(
  id integer NOT NULL DEFAULT nextval('city_id_seq'::regclass),
  description text NOT NULL,
  CONSTRAINT "CityKey" PRIMARY KEY (id)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE public.city
  OWNER TO postgres;
