-- Table: public.person

-- DROP TABLE public.person;

CREATE TABLE public.person
(
  id integer NOT NULL DEFAULT nextval('person_id_seq'::regclass),
  age integer NOT NULL,
  name text NOT NULL,
  CONSTRAINT "PKey" PRIMARY KEY (id)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE public.person
  OWNER TO postgres;
