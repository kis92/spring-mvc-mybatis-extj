package com.model;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.List;

@XmlRootElement(name = "personsAll")
public class PersonList {

    private List<Person> personList;

    public PersonList() {
        super();
    }

    public PersonList(List<Person> personList) {
        super();
        this.personList = personList;
    }

    @XmlElement(name = "persons")
    public List<Person> getPersonList() {
        return personList;
    }

    public void setPersonList(List<Person> personList) {
        this.personList = personList;
    }

}