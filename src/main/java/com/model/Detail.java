package com.model;

import javax.xml.bind.annotation.XmlElement;

/**
 * Created or updated by KiS92 on 19.10.2016:22:38.
 */
public class Detail {
    private int id;
    private String name;

    @XmlElement
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @XmlElement
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
