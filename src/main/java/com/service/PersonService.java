package com.service;

import com.dao.PersonDAO;
import com.model.Person;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class PersonService {

    private PersonDAO personDAO;

    /**
     * Get all persons
     *
     * @return
     */
    @Transactional(readOnly = true)
    public List<Person> getPersonList() {
        return personDAO.getPersons();
    }

    @Transactional(readOnly = true)
    public Person getPerson(int id) {
        return personDAO.getPerson(id);
    }

    @Transactional
    public Person create(Person data) {
        return personDAO.savePerson(data);
    }

    /**
     * Update contact
     */
    @Transactional
    public Person update(Person person) {
        return personDAO.updatePerson(person);
    }

    /**
     * Delete contact
     *
     * @param data - json data from request
     */
    @Transactional
    public void delete(Object data) {
        Integer id = Integer.parseInt(data.toString());
        personDAO.deletePerson(id);
    }


    @Autowired
    public void setPersonDAO(PersonDAO personDAO) {
        this.personDAO = personDAO;
    }

}
