package com.dao;

import com.model.Person;

import java.util.List;

public interface PersonDAO {

    List<Person> getPersons();

    void deletePerson(int id);

    Person getPerson(int id);

    Person savePerson(Person person);

    Person updatePerson(Person person);

}
