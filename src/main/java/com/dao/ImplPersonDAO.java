package com.dao;

import com.model.Person;
import org.apache.ibatis.session.SqlSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;


@Repository
public class ImplPersonDAO implements PersonDAO {

    @Autowired
    private SqlSession sqlSession;

    /**
     * Get List of contacts from database
     *
     * @return list of all contacts
     */
    @SuppressWarnings("unchecked")
    @Override
    public List<Person> getPersons() {
//        return sqlSession.selectList("data-mapper-person.selectAllPeople");
        return sqlSession.selectList("data-mapper-person.selectPersons");
    }

    /**
     * Delete a person with the id passed as parameter
     *
     * @param id
     */
    @Override
    public void deletePerson(int id) {
        sqlSession.delete("data-mapper-person.deletePerson", id);
    }

    @Override
    public Person getPerson(int id) {
        return sqlSession.selectOne("data-mapper-person.selectOnePerson", id);
    }

    @Override
    public Person savePerson(Person person) {
        sqlSession.insert("data-mapper-person.insertPerson", person);
        return person;
    }

    @Override
    public Person updatePerson(Person person) {
        sqlSession.update("data-mapper-person.updatePerson", person);
        return person;
    }

}
