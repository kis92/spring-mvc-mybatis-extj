package com.dao;

import com.model.City;

import java.util.List;

/**
 * Created or updated by KiS92 on 16.10.2016:20:24.
 */
public interface CityDAO {
    List<City> getAllCities();
    City getCityById(int id);
    City createCity(City city);
    City updateCity(City city);
    void deleteCity(int id);

}
