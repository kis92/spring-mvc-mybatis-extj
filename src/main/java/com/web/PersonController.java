package com.web;

import com.model.Person;
import com.model.PersonList;
import com.service.PersonService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.xml.bind.JAXBException;
import java.util.List;
import java.util.logging.Logger;

@Controller
public class PersonController {
    public static final String LINK_SUBMIT = "linkSubmit";
    public static final String EDIT = "edit";
    public static final String ADD = "add";
    public static final String PERSON_DATA = "personData";

    private final Logger logger = Logger.getLogger(getClass().getName());

    @Autowired
    private PersonService personService;

    @RequestMapping(value = "/xmls", method = RequestMethod.GET)
    public @ResponseBody  PersonList getPersonsInXML() throws JAXBException {
        logger.entering("getPersonInXML","/xmls");
        List<Person> persons = personService.getPersonList();
        PersonList personList = new PersonList();
        personList.setPersonList(persons);
        return personList;
    }

    // RequestMapping value accepts a list, use that if a controller needs multiple request map
    @RequestMapping(value = {"/", "/list"}, method = RequestMethod.GET)
    public String list(Model model) {
        // Call MyBatis mapper to get data from DB
        List<Person> persons = personService.getPersonList();
        // Add retrieved data to our model
        model.addAttribute(PERSON_DATA, persons);
        return "list";
    }

    @RequestMapping(value = {"/form", "/edit"}, method = RequestMethod.GET)
    public String form(@RequestParam(value = "id", required = false) Integer personId, Model model) {
        // Check whether this is an add or edit request
        Person person;
        if (personId != null) {
            person = personService.getPerson(personId);
            model.addAttribute(LINK_SUBMIT, EDIT);
        } else {
            person = new Person();
            model.addAttribute(LINK_SUBMIT, ADD);
        }
        model.addAttribute(PERSON_DATA, person);
        return "form";
    }

    @RequestMapping(value = "/add", method = RequestMethod.POST)
    // Retrieve POST data by adding @ModelAttribute parameter
    public String add(@ModelAttribute("SpringWeb") Person person, Model model) {
        personService.create(person);
        return "redirect:/list";
    }

    @RequestMapping(value = "/edit", method = RequestMethod.POST)
    public String edit(@ModelAttribute("SpringWeb") Person person, Model model) {
        personService.update(person);
        return "redirect:/list";
    }

    @RequestMapping(value = "/delete/{personId}")
    public String delete(@PathVariable("personId") Integer personId) {
        personService.delete(personId);
        return "redirect:/list";
    }
}